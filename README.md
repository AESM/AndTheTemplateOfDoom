<!-- README.md -->

AndTheTemplateOfDoom
===============================================================================

AndTheTemplateOfDoom is a git and GitHub repo template, which generates skeleton files for new projects.

The skeleton files are:

- A `README.md` file for documentation
- An extensive `.gitignore` file
- An `.editorconfig` file
- An MIT `LICENSE`
- A `humans.txt` file for crediting contributors
- A `robots.txt` file for giving site instructions to web robots

## Contents

- [Title](#andthetemplateofdoom)
- [Instructions](#instructions)
- [Team](#team)
- [License](#license)
- [Connect](#connect)

## Instructions

Clone the [AndTheTemplateOfDoom](https://github.com/SuitAndCape/AndTheTemplateOfDoom) repo, add the `gitit` alias to your shell profile, make alias modifications if necessary, modify or delete the scaffold files to your liking, clone a repo/create/go to the directory of your new project, run the `gitit` command in the repo's local directory, then modify the project files.

### Clone AndTheTemplateOfDoom
Make a local clone of the AndTheTemplateOfDoom repo.  The alias below is designed to look for the clone in the custom `~/Code` directory.

### Create Alias
Copy the following code into your shell profile (`~/.bash_profile`, `~/.bashrc`, `~/.zsh`, etc.) and modify it as needed:

``` sh
## NOTE: must have `AndTheTemplateOfDoom` repo cloned into the ~/Code
#directory

## SOURCE: github.com/SuitAndCape/AndTheTemplateOfDoom
## `gitit` – Loads `AndTheTemplateOfDoom` default local Git repo files
alias gitit="rsync -av --exclude=\".DS_Store\" --exclude=\".git\"
~/Code/AndTheTemplateOfDoom/. $PWD/"
```

### Modify Templates
View the `README.md`, `LICENSE` (MIT), `.gitignore`, `.editorconfig`, `robots.txt`, and `humans.txt` files.  These will be the skeleton files that make up your git and GitHub scaffolds.

Remove the files you don't plan on ever using, add files you frequently require, replace files with ones that are more relevant, and modify the remaining files to create your own personal scaffold.

It is recommended that these template files remain vague.  Instead, each time you begin a new project, fill-out the specific information in the newly generated files.

### Generate Skeleton Files
Setup and go to your new project's local directory, then generate the skeleton files by running the following command:

``` sh
gitit
```

### Modify Skeleton Files
After generating the skeleton files, edit them to specifically support your project.

#### robots.txt Skeleton
The `robots.txt` file helps developers enforce _[The Robots Exclusion Protocol](http://www.robotstxt.org/robotstxt.html)_.  This isn't a foolproof system, but it's a good practice to include and customize this file.  For more information, check out [robotstxt.org](http://robotstxt.org/).

#### humans.txt Skeleton
The `humans.txt` file is part of the [humanstxt.org](http://humanstxt.org/) initiative, aiming to put the spotlight on the main authors and contributors of websites, apps, and projects.  It contains details like titles, roles, technologies, and links.

#### MIT LICENSE Skeleton
The `LICENSE` file is your project's MIT License.  Include your ownership information.

#### README Skeleton
The `README.md` file is your project's documentation file.  Write a clear and informative `README.md` file so that people will know how they are supposed to use the project.

Be sure to properly modify all links below to fit your project and team.

-------------------------------------------------------------------------------

## Team

[The humans responsible and technology colophon](https://github.com/USERNAME/REPONAME/blob/master/humans.txt).

- **Name** _(role)_: [USERNAME](https://github.com/USERNAME)
- **Name** _(role)_: [USERNAME](https://github.com/USERNAME)
- **Name** _(role)_: [USERNAME](https://github.com/USERNAME)
- ...

## License

This [project](#title) is copyright © 20XX-20XX NAME | ORGANIZATION.  It is free software that may be redistributed under the terms specified in the [LICENSE](https://github.com/USERNAME/REPONAME/blob/master/LICENSE).

This is based on [The MIT License (MIT)](http://opensource.org/licenses/MIT).  For more information, visit the [Open Source Initiative](http://opensource.org/) website.

## Connect

|              :clipboard:             |                :link:                |
| ------------------------------------ | ------------------------------------ |
**_ORGANIZATION GitHub_** | https://github.com/ORGANIZATION
**_Personal GitHub_**     | https://github.com/USERNAME
**_Website_**             | https://WEBSITE/
**_LinkedIn_**            | https://www.linkedin.com/in/USERNAME
**_Dribbble_**            | https://dribbble.com/USERNAME
**_Twitter_**             | https://twitter.com/USERNAME
